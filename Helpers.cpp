﻿#include <iostream>

void PrintEvenOrOddNumbers(int amountNumbers, bool isEven)
{
    for (int i = 0; i <= amountNumbers; ++i)
    {
        if (i % 2 == 0 && isEven)
            std::cout << i << std::ends;
        else if (i % 2 != 0 && !isEven)
            std::cout << i << std::ends;
    }
}
