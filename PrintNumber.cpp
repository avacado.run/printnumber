#include <iostream>
#include "Helpers.h"

// isEven = true: print even numbers
// isEven = false: print odd numbers

int main(int argc, char* argv[])
{
    constexpr int AMOUNT_NUMBERS = 10;
    constexpr bool isEven = false;
    
    for (int i = 0; i <= AMOUNT_NUMBERS; ++i)
    {
        if (i % 2 == 0)
            std::cout << i << std::ends;
    }
    std::cout << std::endl;
    
    PrintEvenOrOddNumbers(AMOUNT_NUMBERS, isEven);
    
    return 0;
}